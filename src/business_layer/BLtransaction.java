/**
@author Abhineet Gupta (S11058338)
@author Wafaa Wardah (S11042586)
@author Ravishel Naicker (S11098100)

CS240: Software Engineering
@since Sem 1, 2014 @ USP
Library Asset Management Enabler (LAME) System
*/

package business_layer;

import data_access_layer.DAtransaction;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;

/**
 * BL entity class for book loans and returns i.e. transactions
 * @author Abhineet
 */
public class BLtransaction {
    private String date_borrowed;
    private String date_expected;
    private String date_returned;
    private String sid;
    private String bcid;
    private String book_title;

    public String getDate_borrowed() {
        return date_borrowed;
    }

    public void setDate_borrowed(String date_borrowed) {
        this.date_borrowed = date_borrowed;
    }

    public String getDate_expected() {
        return date_expected;
    }

    public void setDate_expected(String date_expected) {
        this.date_expected = date_expected;
    }

    public String getDate_returned() {
        return date_returned;
    }

    public void setDate_returned(String date_returned) {
        this.date_returned = date_returned;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getBcid() {
        return bcid;
    }

    public void setBcid(String bcid) {
        this.bcid = bcid;
    }

    public String getBook_title() {
        return book_title;
    }

    public void setBook_title(String book_title) {
        this.book_title = book_title;
    }

    /**
     * Retrieves a list of transactions given a date range
     * @param dateTo
     * @param dateFrom
     * @return
     * @throws SQLException 
     */
    public DefaultTableModel retrieve(String dateTo, String dateFrom) throws SQLException {
        DAtransaction t = new DAtransaction();
        Vector<Vector<String>> results = t.retrieve(dateTo, dateFrom);
        
        Vector<String> columns = new Vector<>();
        columns.add("Book Copy ID");
        columns.add("Book Title");
        columns.add("Student ID");
        columns.add("Borrowed");
        columns.add("Expected");
        columns.add("Returned");
        
        DefaultTableModel result = new DefaultTableModel(results, columns);
        return result;
    }
    
    /**
     * Retrieves a list of book transactions as a vector
     * @param dateTo
     * @param dateFrom
     * @return
     * @throws SQLException 
     */
    public Vector retrieveVect(String dateTo, String dateFrom) throws SQLException {
        DAtransaction t = new DAtransaction();
        return t.retrieve(dateTo, dateFrom);
    }
}