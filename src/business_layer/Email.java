/**
@author Abhineet Gupta (S11058338)
@author Wafaa Wardah (S11042586)
@author Ravishel Naicker (S11098100)

CS240: Software Engineering
@since Sem 1, 2014 @ USP
Library Asset Management Enabler (LAME) System
*/

package business_layer;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


/**
 * Class for sending an email
 * @author Abhineet
 */
public class Email {
        
    private String fromEmail;
    private String password;
    private String toEmail;
    private String subject;
    private String body;
    private String fromName;
    
    Properties props;
    Authenticator auth; //create Authenticator object to pass in Session.getInstance argument
    Session session;
    
    /**
     * Setter for recipient's email address
     * @param toEmail
     */
    public void setToEmail(String toEmail) {
        this.toEmail = toEmail;
    }

    /**
     * Setter for email subject
     * @param subject
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * Setter for email body
     * @param body
     */
    public void setBody(String body) {
        this.body = body;
    }
    
    /**
     * Constructor - initializing email 'from' address and credentials
     */
    public Email(){
        fromEmail = "fste.smart@gmail.com";
        password = "5martA$$";
        fromName = "USP Library";
        
        toEmail = "s11058338@student.usp.ac.fj";
        subject = "Default Email";
        body = "This is the default body of an email from LAME.";
        
        props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
        props.put("mail.smtp.port", "587"); //TLS Port
        props.put("mail.smtp.auth", "true"); //enable authentication
        props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS
        
        auth = new Authenticator() {
            //override the getPasswordAuthentication method
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(fromEmail, password);
            }
        };
        session = Session.getInstance(props, auth);
    }
    
    /**
     * Send an email assuming all initial parameters have been initialized
     * @throws MessagingException
     * @throws UnsupportedEncodingException
     */
    public void sendEmail() throws MessagingException, UnsupportedEncodingException {
        MimeMessage msg;
        msg = new MimeMessage(session);
          
        //set message headers
        msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
        msg.addHeader("format", "flowed");
        msg.addHeader("Content-Transfer-Encoding", "8bit");
 
        msg.setFrom(new InternetAddress(fromEmail, fromName));
        msg.setReplyTo(InternetAddress.parse(fromEmail, false));
        msg.setSubject(subject, "UTF-8");
        msg.setText(body, "UTF-8");
        msg.setSentDate(new Date());
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
        
        Transport.send(msg);
    }
}