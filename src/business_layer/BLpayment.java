/**
@author Abhineet Gupta (S11058338)
@author Wafaa Wardah (S11042586)
@author Ravishel Naicker (S11098100)

CS240: Software Engineering
@since Sem 1, 2014 @ USP
Library Asset Management Enabler (LAME) System
*/
package business_layer;

import data_access_layer.DApayment;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 * BL Entity class for Library Payments
 * @author Abhineet
 */
public class BLpayment {
    private String date;
    private String sid;
    private Double amount;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * Retrieves all payments made during a certain period
     * @param dateTo
     * @param dateFrom
     * @return a Table model with the three columns
     * @throws SQLException 
     */
    public TableModel retrieve(String dateTo, String dateFrom) throws SQLException {
        DApayment p = new DApayment();
        Vector<Vector<String>> results = p.retrieve(dateTo, dateFrom);
        
        Vector<String> columns = new Vector<>();
        columns.add("Date");
        columns.add("Student ID");
        columns.add("Amount");
        
        DefaultTableModel result = new DefaultTableModel(results, columns);
        return result;
    }

    /**
     * Calculate Net Income of the Library in a given period
     * @param dateTo
     * @param dateFrom
     * @return
     * @throws SQLException 
     */
    public static String calcNetIncome(String dateTo, String dateFrom) throws SQLException {
        DApayment p = new DApayment();
        return p.calcNetIncome(dateTo, dateFrom);
    }
    
    /**
     * Retrieves payments during a given period as a vector
     * @param dateTo
     * @param dateFrom
     * @return
     * @throws SQLException 
     */
    public Vector retrieveVect(String dateTo, String dateFrom) throws SQLException {
        DApayment p = new DApayment();
        return p.retrieve(dateTo, dateFrom);
    }
}
