/**
@author Abhineet Gupta (S11058338)
@author Wafaa Wardah (S11042586)
@author Ravishel Naicker (S11098100)

CS240: Software Engineering
@since Sem 1, 2014 @ USP
Library Asset Management Enabler (LAME) System
*/

package business_layer;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Interface for interacting with Springer database
 * @author Abhineet
 */
public interface API_Springer {
    String url = "localhost:3306/springer";
    
    /**
     * Searching for articles
     * @param keyword
     * @return ArrayList of articles
     * @throws SQLException 
     */
    ArrayList searchByKeyword(String keyword) throws SQLException;
    
    /**
     * Gets specific article
     * @return String - file location
     */
    String getArticle();
    
}
