/**
@author Abhineet Gupta (S11058338)
@author Wafaa Wardah (S11042586)
@author Ravishel Naicker (S11098100)

CS240: Software Engineering
@since Sem 1, 2014 @ USP
Library Asset Management Enabler (LAME) System
*/

package business_layer;

import data_access_layer.DAbookTitle;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import javax.mail.MessagingException;

/**
 * Entity for a Book Title (Business layer)
 * @author Abhineet
 */
public class BLbookTitle 
{
    private Integer bookID;
    private String title;
    private String author;
    private String publisher;
    private String catalogueNum;

    /**
     * Getter for Book ID
     * @return BookID
     */
    public Integer getBookID() {
        return bookID;
    }

    /**
     * Setter for Book ID
     * @param bookID
     */
    public void setBookID(Integer bookID) {
        this.bookID = bookID;
    }

    /**
     * Getter for Book Title
     * @return Title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Setter for Book Title
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Getter for Book Author
     * @return author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Setter for Book Author
     * @param author
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * Getter for book publisher
     * @return publisher
     */
    public String getPublisher() {
        return publisher;
    }

    /**
     * Setter for book publisher
     * @param publisher
     */
    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    /**
     * Getter for book catalog number
     * @return catalog number (string)
     */
    public String getCatalogueNum() {
        return catalogueNum;
    }

    /**
     * Setter for book catalog number
     * @param catalogueNum
     */
    public void setCatalogueNum(String catalogueNum) {
        this.catalogueNum = catalogueNum;
    }
    
    /**
     * Searches for the list of books that match a given keyword.
     * The keyword is searched in ALL attributed of a book i.e. title, author etc.
     * @param keyword
     * @return All books that match the keyword
     * @throws SQLException
     */
    public ArrayList<BLbookTitle> searchBook(String keyword) throws SQLException
    {
        DAbookTitle books = new DAbookTitle();
        return books.search(keyword);
    }

    /**
     * Checks whether this book can be reserved
     * True only if there are no book copies in the library currently
     * @return True, if the book can be reserved
     * @throws SQLException
     */
    public Boolean canBeReserved() throws SQLException {
        DAbookTitle books = new DAbookTitle();
        return books.canBeReserved(this.getBookID());
    }

    /**
     * Reserves this book to the current student
     * @throws SQLException
     */
    public void reserve() throws SQLException {
        DAbookTitle books = new DAbookTitle();
        books.reserve(this.getBookID());
    }

    /**
     * Places an inter-library loan request
     * @param shipCost - how much does shipping cost?
     * @throws SQLException 
     */
    public void placeOrder(Integer shipCost) throws SQLException {
        DAbookTitle book = new DAbookTitle();
        book.placeOrder(this.getBookID(), -1 * shipCost);
    }
    
    /**
     * Returns a list of all inter-library requested books
     * @return A vector of vector of strings containing info
     * @throws SQLException 
     */
    public static Vector getRequestedBooks() throws SQLException{
        DAbookTitle daBookT = new DAbookTitle();
        return daBookT.getRequestedBooks();
    }
    
    /**
     * Approve an inter-library loan request
     * @param req_id - request ID
     * @param sid - student ID
     * @throws MessagingException
     * @throws UnsupportedEncodingException
     * @throws SQLException 
     */
    public void approveRequest(Integer req_id, String sid) 
            throws MessagingException, UnsupportedEncodingException, 
            SQLException {
        
        Email email = new Email();
        email.setToEmail(sid + "@student.usp.ac.fj");
        email.setSubject("Request for Inter-library Loan Approved");
        email.setBody("Dear " + sid + ",\n\nYour book titled:\n\t'"
                        + this.getTitle() + "'\nhas arrived in the library. "
                + "Please come and check it out in the normal fashion."
                + "\n\nPlease do not respond to this email; "
                + "sent via the USP Library LAME system.");    
        email.sendEmail();
        
        DAbookTitle daBook = new DAbookTitle();
        daBook.approveRequest(req_id);
    }
    
    /**
     * Deny an inter-library request
     * @param req_id
     * @param sid
     * @throws MessagingException
     * @throws UnsupportedEncodingException
     * @throws SQLException 
     */
    public void denyRequest(Integer req_id, String sid) throws MessagingException, UnsupportedEncodingException, SQLException {
        
        Email email = new Email();
        email.setToEmail(sid + "@student.usp.ac.fj");
        email.setSubject("Request for Inter-library Loan Rejected");
        email.setBody("Dear " + sid + ",\n\nYour inter-library request for book titled:\n\t'"
                        + this.getTitle() + "\nhas been rejected by the library. Please visit us for further enquiry."
                    + "\n\nPlease do not respond to this email; sent via the LAME system.");    
        email.sendEmail();
        
        DAbookTitle daBook = new DAbookTitle();
        daBook.denyRequest(req_id);
    }
}
