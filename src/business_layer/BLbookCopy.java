/**
@author Abhineet Gupta (S11058338)
@author Wafaa Wardah (S11042586)
@author Ravishel Naicker (S11098100)

CS240: Software Engineering
@since Sem 1, 2014 @ USP
Library Asset Management Enabler (LAME) System
*/

package business_layer;

import data_access_layer.DAbookCopy;
import data_access_layer.DAbookTitle;
import java.sql.SQLException;
import java.util.Vector;


/**
 * Business layer entity simulating a Book Copy
 * Derived from a Book Title
 * @author Abhineet
 */
public class BLbookCopy extends BLbookTitle{
    private Integer copyID;
    private Integer edition;
    private Integer pubYear;
    private String ISBN;
    private Integer numCopies;
    private int bcid;


    /**
     * Returns number of copies of this book being entered
     * @return Number of copies (Integer)
     */
    public Integer getNumCopies() {
        return numCopies;
    }

    /**
     * Sets number of copies of this book
     * @param numCopies
     */
    public void setNumCopies(Integer numCopies) {
        this.numCopies = numCopies;
    }

    /**
     * Get Edition number of this book copy
     * @return Edition
     */
    public Integer getEdition() {
        return edition;
    }

    /**
     * Sets edition number for this book
     * @param edition
     */
    public void setEdition(Integer edition) {
        this.edition = edition;
    }

    /**
     * Gets Publication Year of the book
     * @return publication year
     */
    public Integer getPubYear() {
        return pubYear;
    }

    /**
     * Sets Publication Year of the book
     * @param pubYear
     */
    public void setPubYear(Integer pubYear) {
        this.pubYear = pubYear;
    }

    /**
     * Gets ISBN number of the book
     * @return ISBN
     */
    public String getISBN() {
        return ISBN;
    }

    /**
     * Sets ISBN of the book
     * @param ISBN
     */
    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }
    
    /**
     * Gets the Copy ID of this book copy
     * @return Copy ID
     */
    public Integer getCopyID() {
        return copyID;
    }

    /**
     * Sets the Copy ID of this book
     * @param copyID
     */
    public void setCopyID(Integer copyID) {
        this.copyID = copyID;
    }
    
    /**
     * Getter for Book Copy ID
     * @return book copy ID
     */
    public int getBcid() {
        return bcid;
    }

    /**
     * Setter for Book Copy ID
     * @param bcid 
     */
    public void setBcid(int bcid) {
        this.bcid = bcid;
    }

    /**
     * Add this new book into the system
     * @throws SQLException
     */
    
    public void add() throws SQLException{
        DAbookTitle daBook = new DAbookTitle();
        daBook.add(this);
    }

    /**
     * Get a list of ALL reserved books in the library
     * @return Vector list of reserved book copies
     * @throws SQLException
     */
    public Vector getReservedBooks() throws SQLException {
        DAbookCopy book = new DAbookCopy();
        return book.getReservedBooks();
    }

    /**
     * Check in a book back into the library with the given Book Copy ID
     * @param bcid
     * @throws SQLException
     */
    public void check_in(Integer bcid) throws SQLException {
        DAbookCopy book = new DAbookCopy();
        book.check_in(bcid);
        
    }

    /**
     * Check out a book (given book copy ID) to a student (given student ID)
     * @param bcid
     * @param sid
     * @param loanPeriod
     * @throws SQLException
     */
    public void checkOut(Integer bcid, String sid, Integer loanPeriod) throws SQLException {
        DAbookCopy book = new DAbookCopy();
        book.checkOut(bcid, sid, loanPeriod);
    }

    /**
     * Delete a book reservation given the book copy ID
     * @param bcid_delete
     * @throws SQLException
     */
    public void deleteReservation(Integer bcid_delete) throws SQLException {
        DAbookCopy book = new DAbookCopy();
        book.deleteReservation(bcid_delete);
    }

    /**
     * Delete this book from the system
     * @throws SQLException
     */
    public void delete() throws SQLException {
        DAbookCopy book = new DAbookCopy();
        book.delete(this.getCopyID());
    }

    /**
     * Get the next Book Copy ID in the database
     * @return the next BCID as an Integer
     * @throws SQLException
     */
    public Integer getNextBCID() throws SQLException {
        DAbookCopy book = new DAbookCopy();
        return book.getNextBCID(this);
    }

    /**
     * Checks whether a given book copy is actually checked out or not
     * @return true if book is checked out
     * @throws java.sql.SQLException
     */
    public boolean isCheckedOut() throws SQLException {
        DAbookCopy book = new DAbookCopy();
        return book.isCheckedOut(this);
    }
}