/**
@author Abhineet Gupta (S11058338)
@author Wafaa Wardah (S11042586)
@author Ravishel Naicker (S11098100)

CS240: Software Engineering
@since Sem 1, 2014 @ USP
Library Asset Management Enabler (LAME) System
*/

package business_layer;

import data_access_layer.DAextJarticle;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * BL Entity class for an external journal article; implements ext-DB APIs
 * @author Abhineet
 */
public class BLextJarticle implements API_IEEE, API_Springer{
    
    private String j_id;
    private String j_s_title;
    private String j_l_title;
    private String j_pub_year;
    private String j_impact_factor;
    private String j_editor_IC;
    
    private String id;
    private String title;
    private String authors;
    private String date_published;
    private String file_location;
    
    private final String db;

    /**
     * Initialize with the Database type
     * @param db
     */
    public BLextJarticle(String db) {
        this.db = db;
    }

    /**
     * Searches Databases via keyword
     * @param keyword
     * @return
     * @throws SQLException 
     */
    @Override
    public ArrayList<BLextJarticle> searchByKeyword(String keyword) throws SQLException{
        DAextJarticle article = new DAextJarticle(db);
        return article.search(keyword);
    }

    @Override
    public String getArticle() {
        return this.getFile_location();
    }

    /**
     * Getter for Journal ID
     * @return
     */
    public String getJ_id() {
        return j_id;
    }

    /**
     * Setter for Journal ID
     * @param j_id
     */
    public void setJ_id(String j_id) {
        this.j_id = j_id;
    }

    /**
     * Getter for Journal's Short title
     * @return
     */
    public String getJ_s_title() {
        return j_s_title;
    }

    /**
     * Setter for Journal's Short Title
     * @param j_s_title
     */
    public void setJ_s_title(String j_s_title) {
        this.j_s_title = j_s_title;
    }

    /**
     * Getter for Journal's Long title
     * @return
     */
    public String getJ_l_title() {
        return j_l_title;
    }

    /**
     * Setter
     * @param j_l_title
     */
    public void setJ_l_title(String j_l_title) {
        this.j_l_title = j_l_title;
    }

    /**
     * Getter
     * @return
     */
    public String getJ_pub_year() {
        return j_pub_year;
    }

    /**
     * Setter
     * @param j_pub_year
     */
    public void setJ_pub_year(String j_pub_year) {
        this.j_pub_year = j_pub_year;
    }

    /**
     * Getter
     * @return
     */
    public String getJ_impact_factor() {
        return j_impact_factor;
    }

    /**
     * Setter
     * @param j_impact_factor
     */
    public void setJ_impact_factor(String j_impact_factor) {
        this.j_impact_factor = j_impact_factor;
    }

    /**
     * Getter
     * @return
     */
    public String getJ_editor_IC() {
        return j_editor_IC;
    }

    /**
     * Setter
     * @param j_editor_IC
     */
    public void setJ_editor_IC(String j_editor_IC) {
        this.j_editor_IC = j_editor_IC;
    }

    /**
     * Getter
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     * Setter
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Getter
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     * Setter
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Getter
     * @return
     */
    public String getAuthors() {
        return authors;
    }

    /**
     * Setter
     * @param authors
     */
    public void setAuthors(String authors) {
        this.authors = authors;
    }

    /**
     * Getter
     * @return
     */
    public String getDate_published() {
        return date_published;
    }

    /**
     * Setter
     * @param date_published
     */
    public void setDate_published(String date_published) {
        this.date_published = date_published;
    }

    /**
     * Getter
     * @return
     */
    public String getFile_location() {
        return file_location;
    }

    /**
     * Setter
     * @param file_location
     */
    public void setFile_location(String file_location) {
        this.file_location = file_location;
    }
}