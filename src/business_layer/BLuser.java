/**
@author Abhineet Gupta (S11058338)
@author Wafaa Wardah (S11042586)
@author Ravishel Naicker (S11098100)

CS240: Software Engineering
@since Sem 1, 2014 @ USP
Library Asset Management Enabler (LAME) System
*/

package business_layer;

import data_access_layer.*;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.Vector;
import javax.mail.MessagingException;

/**
 *Details of the current user stored here
 *Only one instance allowed at a time - singleton design pattern 
 * @author Abhineet
 */
public class BLuser 
{
    private String uName;       //username entered    
    private String uPass;       //password entered
    private String type;        //clerk, student, etc.
    private String level;       //postgrad or undergrad (if not staff)
    private Double fineRate;  //fine per day
    
    private static BLuser user = null;  //current user reference

    private BLuser() {}
    
    /**
     * Instantiates a BLuser instance if none exist
     * else, returns reference to current user
     * @return BLuser variable - reference to current instance
     */
    public static BLuser getInst() {
        if (user==null) {
            user = new BLuser();
        }
        return user;
    }
    
    /**
     * Deletes current user object
     */
    public static void removeInst()
    {
        if (user!=null)
            user = null;
    }
    
    /**
     * Gets user's name - Student ID or Clerk ID
     * @return name or ID
     */
    public String getuName() {
        return uName;
    }

    /**
     * Sets user's name (ID)
     * @param uName
     */
    public void setuName(String uName) {
        this.uName = uName;
    }

    /**
     * Gets user's password
     * @return password
     */
    public String getuPass() {
        return uPass;
    }

    /**
     * Sets user's password
     * @param uPass
     */
    public void setuPass(String uPass) {
        this.uPass = uPass;
    }
    
    /**
     * Getter for user type (student, clerk, etc.)
     * @return user type (possible values: "student", "clerk", etc.)
     */
    public String getType() {
        return type;
    }

    /**
     * Sets user type i.e. student, clerk, etc. 
     * @param type
     */
    public void setType(String type){
        this.type = type;
    }
    
    /**
     * Checks if the username and password are stored in the Database i.e. valid
     *
     * @return Boolean; true if login matched, false otherwise
     * @throws java.sql.SQLException
     */
    public Boolean checkCredential() throws SQLException
    {
        DAuser dUser = new DAuser();
        return dUser.checkCredential();
    }
    
    /**
     * Gets a list of loaned books by the current user (student)
     * @return list of books (BLbookCopies)
     * @throws SQLException
     */
    public Vector getLoanedBooks () throws SQLException
    {
        DAuser dUser = new DAuser();
        return dUser.getLoanedBooks();
    }

    /**
     * Gets a list of reserved books by the current user (student)
     * @return list of books (BLbookCopies)
     * @throws SQLException
     */
    public Vector getReservedBooks() throws SQLException
    {
        DAuser dUser = new DAuser();
        return dUser.getReservedBooks();
    }

    /**
     * Calculates fine due by the current user (student)
     * @return Fine (Integer)
     * @throws SQLException
     */
    public Integer calculateFine() throws SQLException {
        DAuser dUser = new DAuser();
        if (this.uName.charAt(0) == 'h' || this.uName.charAt(0) == 'H') {
            this.fineRate = 2.5;
        }
        else {
            this.fineRate = 1.0;
        }
        return dUser.calculateFine(this.getuName(), fineRate);
    }
    
    /**
     * Calculates fine of a patron of the library given the ID
     * @param id - student ID or staff ID
     * @return - fine as a number
     * @throws SQLException
     */
    public static Integer calculateFine(String id) throws SQLException {
        DAuser dUser = new DAuser();
        Double fineRate;
        if (id.charAt(0) == 'h' || id.charAt(0) == 'H') {
            fineRate = 2.5;
        }
        else {
            fineRate = 1.0;
        }
        return dUser.calculateFine(id, fineRate);
    }

    /**
     * Completely pay the fine due for the student whose ID is the current username
     * @param payment
     * @throws SQLException
     */
    public void payFine(Integer payment) throws SQLException {
        DAuser dUser = new DAuser();
        dUser.payFine(this.getuName(), payment);
    }
    
    /**
     * Calculates the total number of books currently checked out to this patron
     * @param id - student ID or staff ID
     * @return - number of books checked out
     * @throws SQLException
     */
    public Integer calcNumBooksCheckedOut (String id) throws SQLException
    {
        DAuser dUser = new DAuser();
        return dUser.calcNumBooksCheckedOut(id);
    }
    
    public String getLevel() throws SQLException {
        DAuser dUser = new DAuser();
        this.level = dUser.getLevel();
        return level;
    }

    public Object getLevel(String sid) throws SQLException {
        DAuser dUser = new DAuser();
        this.level = dUser.getLevel(sid);
        return level;
    }

    /**
     * Tasks to be performed at the end of the day
     * These include sending a reminder email to all patrons who have books
     * due exactly a week later
     * @throws SQLException
     * @throws MessagingException
     * @throws UnsupportedEncodingException
     */
    public void endOfDay() throws SQLException, MessagingException, UnsupportedEncodingException {
        DAuser dUser = new DAuser();
        Vector<Vector<String>> patrons = dUser.getUsersToBeReminded();
        
        for (int i = 0; i < patrons.size(); i++) {
            Email email = new Email();
            
            if (patrons.get(i).get(0).charAt(0) == 'h' || patrons.get(i).get(0).charAt(0) == 'H') {
                email.setToEmail("s11058338@student.usp.ac.fj");
            }
            else if (patrons.get(i).get(0).equals("sPG")) {
                email.setToEmail("s11058338@student.usp.ac.fj");
            }
            else{
                email.setToEmail(patrons.get(i).get(0) + "@student.usp.ac.fj");
            }
            
            email.setSubject("Reminder for Upcoming Book-return Deadline");
            email.setBody("Dear " + patrons.get(i).get(0) + ","
                    + "\n\nJust a friendly reminder that your book titled:\n\t'"
                            + patrons.get(i).get(2) + "'\n"
                    + "is due for return in 7 days time (" + patrons.get(i).get(1) + "). "
                    + "Please ensure its return on time to avoid overdue fines."
                    + "\n\n---Please do not respond to this email; "
                    + "sent via the USP Library LAME system.---");
            email.sendEmail();
        }
    }
}