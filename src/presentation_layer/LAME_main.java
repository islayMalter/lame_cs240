/**
@author Abhineet Gupta (S11058338)
@author Wafaa Wardah (S11042586)
@author Ravishel Naicker (S11098100)

CS240: Software Engineering
@since Sem 1, 2014 @ USP
Library Asset Management Enabler (LAME) System
*/

package presentation_layer;

/**
 *  Application Main class
 * @author Abhineet
 */
public class LAME_main 
{    
    /**
     * @param args the command line arguments - none required
     */
    @SuppressWarnings("empty-statement")
    public static void main(String[] args) 
    {        
        Main_Window.main();
    }       
}