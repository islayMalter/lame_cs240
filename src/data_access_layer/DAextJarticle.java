/**
@author Abhineet Gupta (S11058338)
@author Wafaa Wardah (S11042586)
@author Ravishel Naicker (S11098100)

CS240: Software Engineering
@since Sem 1, 2014 @ USP
Library Asset Management Enabler (LAME) System
*/

package data_access_layer;

import business_layer.API_IEEE;
import business_layer.API_Springer;
import business_layer.BLextJarticle;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * DA Entity for an External Journal Article
 * @author Abhineet
 */
public class DAextJarticle {
    MySQL_JDBC db;
    String sql;
    Statement st;
    ResultSet rs;
    private final String extDB;

    public DAextJarticle(String extdb) {
        this.extDB = extdb;
        db = new MySQL_JDBC();
        if (extDB.equals("IEEE")) {
            db.setUrl(API_IEEE.url);
        }
        else{
            db.setUrl(API_Springer.url);
        }
    }

    /**
     * Searches for articles via keyword
     * @param keyword
     * @return
     * @throws SQLException 
     */
    public ArrayList<BLextJarticle> search(String keyword) throws SQLException {
        ArrayList<BLextJarticle> list = new ArrayList<>();
        db.connect();
        st = db.getConnect().createStatement();
        
        sql = "SELECT j.id, j.s_title, j.l_title, j.pub_year, "
                + "j.impact_factor, j.editor_in_chief, "
                + "a.id, a.title, a.authors, a.date_published, a.file_location "
                + "FROM articles a, journals j "
                + "WHERE a.jid = j.id AND "
                + "(j.s_title LIKE '%" + keyword + "%' OR "
                + "j.l_title LIKE '%" + keyword + "%' OR "
                + "a.title LIKE '%" + keyword + "%');";
        
        rs = st.executeQuery(sql);
        
        while (rs.next()){
            BLextJarticle temp = new BLextJarticle(extDB);
            temp.setJ_id(rs.getString(1));
            temp.setJ_s_title(rs.getString(2));
            temp.setJ_l_title(rs.getString(3));
            temp.setJ_pub_year(rs.getString(4));
            temp.setJ_impact_factor(rs.getString(5));
            temp.setJ_editor_IC(rs.getString(6));
            temp.setId(rs.getString(7));
            temp.setTitle(rs.getString(8));
            temp.setAuthors(rs.getString(9));
            temp.setDate_published(rs.getString(10));
            temp.setFile_location(rs.getString(11));
            list.add(temp);
        }
        st.close();
        db.disconnect();
        return list;
    }
}
