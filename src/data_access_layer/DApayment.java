/**
@author Abhineet Gupta (S11058338)
@author Wafaa Wardah (S11042586)
@author Ravishel Naicker (S11098100)

CS240: Software Engineering
@since Sem 1, 2014 @ USP
Library Asset Management Enabler (LAME) System
*/

package data_access_layer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

/**
 * DA entity for a financial payment
 * @author Abhineet
 */
public class DApayment {
    private final MySQL_JDBC db;
    private String sql;
    private Statement st;
    private ResultSet rs;
    
    public DApayment() {
        this.db = new MySQL_JDBC();
    }

    /**
     * Retrieves all payments made during a certain date range
     * @param dateTo
     * @param dateFrom
     * @return
     * @throws SQLException 
     */
    public Vector<Vector<String>> retrieve(String dateTo, String dateFrom) throws SQLException {
        Vector<Vector<String>> resPay = new Vector<>();
        Vector<String> result;
        
        db.connect();
        st = db.getConnect().createStatement();
        
        sql = "SELECT p.date, p.sid, p.payment FROM payments p "
                + "WHERE p.date >= '" + dateFrom + "' "
                + "AND p.date <= '" + dateTo + "';";
        rs = st.executeQuery(sql);
        
        while (rs.next())
        {
            result = new Vector<>();
            result.add(rs.getString(1));
            result.add(rs.getString(2));
            result.add(rs.getString(3));
            resPay.add(result);
        }
        st.close();
        db.disconnect();
        return resPay;
    }

    /**
     * Calculates Net income to the library given a date range
     * @param dateTo
     * @param dateFrom
     * @return
     * @throws SQLException 
     */
    public String calcNetIncome(String dateTo, String dateFrom) throws SQLException {
        String result;
        db.connect();
        st = db.getConnect().createStatement();
        
        sql = "SELECT ifnull(SUM(p.payment), 0) "
                + "FROM payments p "
                + "WHERE p.date >= '" + dateFrom + "' AND "
                + "p.date <= '" + dateTo + "';";
        rs = st.executeQuery(sql);
        rs.next();
        result = rs.getString(1);
        
        st.close();
        db.disconnect();
        return result;
    }
}
