/**
@author Abhineet Gupta (S11058338)
@author Wafaa Wardah (S11042586)
@author Ravishel Naicker (S11098100)

CS240: Software Engineering
@since Sem 1, 2014 @ USP
Library Asset Management Enabler (LAME) System
*/

package data_access_layer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

/**
 * DA entity for a book transaction
 * @author Abhineet
 */
public class DAtransaction {
    private final MySQL_JDBC db;
    private String sql;
    private Statement st;
    private ResultSet rs;
    
    public DAtransaction() {
        this.db = new MySQL_JDBC();
    }

    /**
     * Retrieve all book transactions given a date range
     * @param dateTo
     * @param dateFrom
     * @return
     * @throws SQLException 
     */
    public Vector retrieve(String dateTo, String dateFrom) throws SQLException {
        Vector<Vector<String>> resTrans = new Vector<>();
        Vector<String> result;
        
        db.connect();
        st = db.getConnect().createStatement();
        
        sql = "SELECT t.Bc_ID, b.Bk_Title, t.Stu_ID, t.Trans_DateBorrowed, "
                + "t.Trans_DateExpected, t.Trans_DateReturned "
                + "FROM transactions t, books b, bookcopies bc "
                + "WHERE t.Bc_ID = bc.Bc_ID AND bc.Bk_ID = b.Bk_ID AND "
                + "((t.Trans_DateBorrowed >= '" + dateFrom + "' AND "
                + "t.Trans_DateBorrowed <= '" + dateTo + "') "
                + "OR (t.Trans_DateReturned >= '" + dateFrom + "' AND "
                + "t.Trans_DateReturned <= '" + dateTo + "')) "
                + "ORDER BY t.Trans_DateBorrowed ASC;";
        rs = st.executeQuery(sql);
        
        while (rs.next())
        {
            result = new Vector<>();
            result.add(rs.getString(1));
            result.add(rs.getString(2));
            result.add(rs.getString(3));
            result.add(rs.getString(4));
            result.add(rs.getString(5));
            result.add(rs.getString(6));
            resTrans.add(result);
        }
        st.close();
        db.disconnect();
        return resTrans;
    }
}