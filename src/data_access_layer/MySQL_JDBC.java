/**
@author Abhineet Gupta (S11058338)
@author Wafaa Wardah (S11042586)
@author Ravishel Naicker (S11098100)

CS240: Software Engineering
@since Sem 1, 2014 @ USP
Library Asset Management Enabler (LAME) System
*/

package data_access_layer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class for connecting to the database
 * @author Abhineet
 */
public class MySQL_JDBC {
    
    private String username;
    private String password;
    private String connectionStr;
    private String url;
    private Connection con;

    /**
     * Constructor - sets login credentials for DB and the connection string
     */
    public MySQL_JDBC() {
        this.username = "skullers_ab";
        this.password = "temppass";
        this.url = "localhost:3306/lame";
        this.connectionStr = "jdbc:mysql://" + url;
    }
    
    /**
     * Connects to the specified database using the given username and password
     * @throws SQLException
     */
    public void connect() throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MySQL_JDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
        con = DriverManager.getConnection(connectionStr, username, password);
    }

    /**
     * Disconnects from the database
     * @throws SQLException
     */
    public void disconnect() throws SQLException{
        con.close();
    }

    /**
     * Gets the connection object
     * @return connection object
     */
    public Connection getConnect(){
    	return this.con;
    }

    /**
     * Gets the connection string
     * @return connection string
     */
    public String getConnectionString() {
            return connectionStr;
    }

    /**
     * Sets the connection string
     * @param connectionString
     */
    public void setConnectionString(String connectionString) {
            this.connectionStr = connectionString;
    }

    /**
     * gets the password to the database
     * @return password
     */
    public String getPassword() {
            return password;
    }

    /**
     * Sets the password to be used
     * @param password
     */
    public void setPassword(String password) {
            this.password = password;
    }

    /**
     * gets the URL of the database server
     * @return
     */
    public String getUrl() {
            return url;
    }

    /**
     * Sets the URL of the database server
     * @param url
     */
    public void setUrl(String url) {
            this.url = url;
            this.connectionStr = "jdbc:mysql://" + url;
    }

    /**
     * Gets the username for connecting to the DB
     * @return username
     */
    public String getUsername() {
            return username;
    }

    /**
     * Sets the Username for logging onto the database
     * @param username
     */
    public void setUsername(String username) {
            this.username = username;
    }
}