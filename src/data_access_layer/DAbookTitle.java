/**
@author Abhineet Gupta (S11058338)
@author Wafaa Wardah (S11042586)
@author Ravishel Naicker (S11098100)

CS240: Software Engineering
@since Sem 1, 2014 @ USP
Library Asset Management Enabler (LAME) System
*/

package data_access_layer;

import business_layer.BLbookCopy;
import business_layer.BLbookTitle;
import business_layer.BLuser;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Entity for a book title (Data access layer)
 * @author Abhineet
 */
public class DAbookTitle 
{
    MySQL_JDBC db;
    String sql;
    Statement st;
    ResultSet rs;
    
    private final BLuser user;
    
    /**
     * Constructor - creates a new database object and gets current user info
     */
    public DAbookTitle() 
    {
        user = BLuser.getInst();
        db = new MySQL_JDBC(); 
    }
    
    /**
     * Searches for the list of books that match a given keyword.
     * @param keyword
     * @return ArrayList of BLbookTitles (book titles)
     * @throws SQLException
     */
    public ArrayList<BLbookTitle> search (String keyword) throws SQLException
    {
        ArrayList<BLbookTitle> list = new ArrayList<>();
        db.connect();
        st = db.getConnect().createStatement();
        
        sql = "SELECT * FROM books b "
                + "WHERE b.Bk_Title LIKE '%" + keyword + "%' "
                + "OR b.Bk_Author LIKE '%" + keyword + "%' "
                + "OR b.Bk_Publisher LIKE '%" + keyword + "%';";
        rs = st.executeQuery(sql);
        
        while (rs.next()) 
        {
            BLbookTitle temp = new BLbookTitle();
            temp.setBookID(rs.getInt(1));
            temp.setTitle(rs.getString(2));
            temp.setAuthor(rs.getString(3));
            temp.setPublisher(rs.getString(4));
            temp.setCatalogueNum(rs.getString(5));
            list.add(temp);
        }
        st.close();
        db.disconnect();
        return list;
    }

    /**
     * Add the current book into the database
     * @param book
     * @throws SQLException
     */
    public void add(BLbookCopy book) throws SQLException{
        db.connect();
        st = db.getConnect().createStatement();
        
        sql = "INSERT into books VALUES "
                + "(null, '" + book.getTitle() + "', "
                + "'" + book.getAuthor() + "', "
                + "'" + book.getPublisher() + "', "
                + "'" + book.getCatalogueNum() + "');";
        st.executeUpdate(sql);
        
        sql = "SELECT MAX(Bk_ID) FROM books;";
        rs = st.executeQuery(sql);
        rs.next();
        
        Integer bookID = rs.getInt(1);
        Integer bcid = book.getNextBCID();
        for (int i = 0; i < book.getNumCopies(); i++) {
            sql = "INSERT INTO bookcopies VALUES "
                    + "(" + bcid++ + ", "
                    + "" + book.getEdition() + ", "
                    + "" + book.getPubYear() + ", "
                    + "'" + book.getISBN() + "', "
                    + "1, " + bookID + ");";
            st.executeUpdate(sql);
        }
        
        st.close();
        db.disconnect();
    }

    /**
     * Checks whether this book can be reserved (no copies in library)
     * @param bookID
     * @return true of false
     * @throws SQLException
     */
    public Boolean canBeReserved(Integer bookID) throws SQLException {
        db.connect();
        st = db.getConnect().createStatement();
        Boolean result;
        
        sql = "SELECT count(*) "
                + "FROM books b NATURAL JOIN bookcopies bc "
                + "WHERE bc.Bc_Status = 1 AND b.Bk_ID = " + bookID + ";";
        rs = st.executeQuery(sql);
        
        rs.next();
        result = rs.getInt(1) == 0;
        
        st.close();
        db.disconnect();
        return result;
    }

    /**
     * Reserves a book copy given the Book ID to the current student
     * @param bookID
     * @throws SQLException
     */
    public void reserve(Integer bookID) throws SQLException {
        db.connect();
        st = db.getConnect().createStatement();
        
        sql = "INSERT INTO reservations VALUES "
                + "(null, CURDATE(), '" + user.getuName() + "', "
                + "(SELECT min(Bc_ID) FROM bookcopies bc "
                + "WHERE bc.Bk_ID = " + bookID + " and bc.Bc_Status = 0));";
        st.executeUpdate(sql);
        
        st.close();
        db.disconnect();
    }

    /**
     * Places an inter-library loan order for the specified book and shipping cost
     * @param bkid - Book ID for which the order is being placed
     * @param shipCost - cost for shipping (depends on the shipping method)
     * @throws SQLException 
     */
    public void placeOrder(Integer bkid, Integer shipCost) throws SQLException {
        db.connect();
        st = db.getConnect().createStatement();
        
        sql = "INSERT INTO interlibloans (sid, bkid) VALUES "
                + "('" + user.getuName() + "', " + bkid + ");";
        st.executeUpdate(sql);
        
        sql = "INSERT INTO payments (sid, payment, date, description) "
                + "VALUES ('" + user.getuName() + "', " 
                + shipCost + ", CURDATE(), 'shipping cost');";
        st.executeUpdate(sql);
        
        st.close();
        db.disconnect();
    }

    /**
     * Gets a list of the requested books by students
     * @return a Vector of a Vector of Strings
     * @throws SQLException 
     */
    public Vector getRequestedBooks() throws SQLException {
        Vector<Vector<String>> reqBooks = new Vector<>();
        Vector<String> result;
        
        db.connect();
        st = db.getConnect().createStatement();
        
        sql = "SELECT i.id, i.sid, b.Bk_Title, i.bkid, i.date_requested "
                + "FROM interlibloans i, books b "
                + "WHERE i.bkid = b.Bk_ID AND i.is_approved = 0;";
        rs = st.executeQuery(sql);
        
        while (rs.next())
        {
            result = new Vector<>();
            result.add(rs.getString(1));
            result.add(rs.getString(2));
            result.add(rs.getString(3));
            result.add(rs.getString(4));
            result.add(rs.getString(5));
            reqBooks.add(result);
        }
        
        st.close();
        db.disconnect();  
        return reqBooks;
    }

    /**
     * Approves an inter-library loan request
     * @param req_id
     * @throws SQLException 
     */
    public void approveRequest(Integer req_id) throws SQLException {
        db.connect();
        st = db.getConnect().createStatement();
        
        sql = "UPDATE interlibloans "
                + "SET is_approved = 1 "
                + "WHERE id = " + req_id + ";";
        st.executeUpdate(sql);
        
        st.close();
        db.disconnect();
    }

    /**
     * Rejects an inter-library loan request
     * @param req_id
     * @throws SQLException 
     */
    public void denyRequest(Integer req_id) throws SQLException {
        db.connect();
        st = db.getConnect().createStatement();
        
        sql = "DELETE FROM interlibloans "
                + "WHERE id = " + req_id + ";";
        st.executeUpdate(sql);
        
        st.close();
        db.disconnect();
    }
}
