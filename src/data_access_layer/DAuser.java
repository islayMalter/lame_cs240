/**
@author Abhineet Gupta (S11058338)
@author Wafaa Wardah (S11042586)
@author Ravishel Naicker (S11098100)

CS240: Software Engineering
@since Sem 1, 2014 @ USP
Library Asset Management Enabler (LAME) System
*/

package data_access_layer;

import business_layer.BLuser;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

/**
 * A user entity (Data access layer)
 * @author Abhineet
 */
public class DAuser 
{
    MySQL_JDBC db;
    String sql;
    Statement st;
    ResultSet rs;
    
    private final BLuser user;

    /**
     * Constructor - creates DB object and gets current user object
     */
    public DAuser() {
        user = BLuser.getInst();
        db = new MySQL_JDBC();        
    }
    
    /**
     * Checks if the username and password are valid
     * @return true or false depending on whether the credentials matched
     * @throws SQLException
     */
    public Boolean checkCredential() throws SQLException
    {
        db.connect();
        st = db.getConnect().createStatement();
        switch (user.getType()) {
            case "Student":
                sql = "SELECT COUNT(*) FROM students s "
                        + "WHERE s.Stu_ID = '" + user.getuName() + "' "
                        + "and s.Stu_Password = '" + user.getuPass() + "';";
                rs = st.executeQuery(sql);
                if(rs.next())
                    return rs.getInt(1) == 1;
                break;
            case "Clerk":
                sql = "SELECT COUNT(*) FROM clerks c "
                        + "WHERE c.Clk_ID = '" + user.getuName() + "' "
                        + "and c.Clk_Password = '" + user.getuPass() + "' "
                        + "and c.type = 'clerk';";
                rs = st.executeQuery(sql);
                if(rs.next()){
                    return rs.getInt(1) == 1;
                }   break;
            case "Manager":
                sql = "SELECT COUNT(*) FROM clerks c "
                        + "WHERE c.Clk_ID = '" + user.getuName() + "' "
                        + "and c.Clk_Password = '" + user.getuPass() + "' "
                        + "and c.type = 'manager';";
                rs = st.executeQuery(sql);
                if(rs.next()){
                    return rs.getInt(1) == 1;
                }   break;
            case "Staff":
                sql = "SELECT COUNT(*) FROM staff s "
                        + "WHERE s.id = '" + user.getuName() + "' "
                        + "and s.password = '" + user.getuPass() + "';";
                rs = st.executeQuery(sql);
                if(rs.next())
                    return rs.getInt(1) == 1;
                break;
        }
        st.close();
        db.disconnect();
        return false;        
    }
    
    /**
     * Gets a list of loaned books by the current user (student)
     * @return Vector of a Vector containing book names as Strings
     * @throws SQLException
     */
    public Vector getLoanedBooks () throws SQLException
    {
        Vector<Vector<String>> resBooks = new Vector<>();
        Vector result;
        
        db.connect();
        st = db.getConnect().createStatement();
        
        sql = "SELECT t.Bc_ID, b.Bk_Title, t.Trans_DateExpected "
                + "FROM transactions t NATURAL JOIN books b "
                + "NATURAL JOIN bookcopies bc "
                + "WHERE t.Trans_DateReturned is null "
                + "AND t.Stu_ID = '" + user.getuName() + "';";
        rs = st.executeQuery(sql);
        
        while (rs.next())
        {
            result = new Vector<>();
            result.add(rs.getString(1));
            result.add(rs.getString(2));
            result.add(rs.getString(3));
            resBooks.add(result);
        }
        st.close();
        db.disconnect();
        return resBooks;
    }

    /**
     * Gets a list of reserved books by the current user (student)
     * @return vector of a vector containing titles of books as strings
     * @throws SQLException
     */
    public Vector getReservedBooks() throws SQLException {
        Vector<Vector<String>> resBooks = new Vector<>();
        Vector result;
        
        db.connect();
        st = db.getConnect().createStatement();
        
        sql = "SELECT bc.Bc_ID, b.Bk_Title, t.Trans_DateExpected "
                + "FROM reservations r, bookcopies bc, books b, transactions t "
                + "WHERE r.Bc_ID = bc.Bc_ID AND b.Bk_ID = bc.Bk_ID "
                + "AND t.Bc_ID = r.Bc_ID "
                + "AND r.Stu_ID = '" + user.getuName() + "' "
                + "GROUP BY Bc_ID ORDER BY t.Trans_DateExpected ASC;";
        rs = st.executeQuery(sql);
        
        while (rs.next())
        {
            result = new Vector<>();
            result.add(rs.getString(1));
            result.add(rs.getString(2));
            result.add(rs.getString(3));
            resBooks.add(result);
        }
        st.close();
        db.disconnect();
        return resBooks;
    }

    /**
     * Calculates fine due by the current user (student)
     * @param sid
     * @param fineRate
     * @return fine
     * @throws SQLException
     */
    public Integer calculateFine(String sid, Double fineRate) throws SQLException {
        Integer fine;
        db.connect();
        st = db.getConnect().createStatement();
        
        sql = "CALL calculateTotalFineDueUsingSID"
                + "('" + sid + "', " + fineRate + ");";
        rs = st.executeQuery(sql);
        rs.next();
        fine = rs.getInt(1);
        
        st.close();
        db.disconnect();
        return fine;
    }

    /**
     * Completely pay the fine due for a student, given his ID
     * @param sid
     * @param payment
     * @throws SQLException
     */
    public void payFine(String sid, Integer payment) throws SQLException{
        db.connect();
        st = db.getConnect().createStatement();
        
        sql = "INSERT INTO payments (sid, payment, date, description) "
                + "VALUES ('" + sid + "', " 
                + payment + ", CURDATE(), 'paid fees');";
        
        st.executeUpdate(sql);
        
        st.close();
        db.disconnect();
    }

    /**
     * DA access level - calculate num of books checked out by a patron
     * @param id - Staff or Student ID
     * @return number of books checked out
     * @throws SQLException
     */
    public Integer calcNumBooksCheckedOut(String id) throws SQLException {
        Integer result = 0;
        sql = "SELECT calculateBooksCheckedOut(?);";
        
        db.connect();
        PreparedStatement pst = db.getConnect().prepareStatement(sql);
        pst.setString(1, id);
        rs = pst.executeQuery();
        if (rs.next()) {
            result = rs.getInt(1);
        }
        pst.close();
        db.disconnect();
        return result;
    }

    public String getLevel() throws SQLException {
        String level = "";
        
        sql = "SELECT s.type FROM students s WHERE s.Stu_ID = ?;";
        
        db.connect();
        PreparedStatement pst = db.getConnect().prepareStatement(sql);
        pst.setString(1, user.getuName());
        rs = pst.executeQuery();
        if (rs.next()) {
            level = rs.getString(1);
        }
        pst.close();
        db.disconnect();
        return level;
        
    }

    public String getLevel(String text) throws SQLException {
        String level = "none";
        
        sql = "SELECT s.type FROM students s WHERE s.Stu_ID = ?;";
        
        db.connect();
        PreparedStatement pst = db.getConnect().prepareStatement(sql);
        pst.setString(1, text);
        rs = pst.executeQuery();
        if (rs.next()) {
            level = rs.getString(1);
        }
        pst.close();
        db.disconnect();
        return level;
    }

    /**
     * Returns list of user who have books due in a week's time
     * @return list of users with books due in 7 days time
     * @throws SQLException
     */
    public Vector<Vector<String>> getUsersToBeReminded() throws SQLException {
        Vector<Vector<String>> rmdUsers = new Vector<>();
        Vector<String> result;
        
        //get list of users with user ID, date expected and book title
        sql = "SELECT t.Stu_ID, t.Trans_DateExpected, b.Bk_Title "
                + "FROM transactions t JOIN bookcopies bc on t.Bc_ID = bc.Bc_ID "
                + "JOIN books b on bc.Bk_ID = b.Bk_ID "
                + "WHERE DATEDIFF(t.Trans_DateExpected, CURDATE()) = 7 "
                + "and t.reminded = 0;";
        db.connect();
        st = db.getConnect().createStatement();
        rs = st.executeQuery(sql);
        
        while (rs.next()){
            result = new Vector<>();
            result.add(rs.getString("Stu_ID"));
            result.add(rs.getString("Trans_DateExpected"));
            result.add(rs.getString("Bk_Title"));
            rmdUsers.add(result);
        }
        
        //remove users who are undergrad students
        int size = rmdUsers.size();
        String type;
        for (int i = 0; i < size; i++) {
            type = getLevel(rmdUsers.get(i).get(0));
            if (type.equals("Undergraduate")) {
                rmdUsers.remove(i);
            }
        }
        
        //set the users status as reminded
        sql = "UPDATE lame.transactions t "
                + "SET t.reminded = 1 "
                + "WHERE DATEDIFF(t.Trans_DateExpected, CURDATE()) = 7;";
        db.connect();
        st = db.getConnect().createStatement();
        st.executeUpdate(sql);
        
        //return a 2D vector of users with borrowed item details for each
        return rmdUsers;
        
    }
}