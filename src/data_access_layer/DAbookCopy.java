/**
@author Abhineet Gupta (S11058338)
@author Wafaa Wardah (S11042586)
@author Ravishel Naicker (S11098100)

CS240: Software Engineering
@since Sem 1, 2014 @ USP
Library Asset Management Enabler (LAME) System
*/

package data_access_layer;

import business_layer.BLbookCopy;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

/**
 * Data access layer entity for a book copy
 * @author Abhineet
 */
public class DAbookCopy
{
    private final MySQL_JDBC db;
    private String sql;
    private Statement st;
    private ResultSet rs;
    
    /**
     * Constructor - creates a database object
     */
    public DAbookCopy() 
    {
        db = new MySQL_JDBC();
    }

    /**
     * Get a list of ALL reserved books in the library
     * @return Vector containing book information as a vector of strings 
     * @throws SQLException
     */
    public Vector getReservedBooks() throws SQLException {
        Vector<Vector<String>> resBooks = new Vector<>();
        Vector<String> result;
        
        db.connect();
        st = db.getConnect().createStatement();
        
        sql = "SELECT bc.Bc_ID, r.Stu_ID, b.Bk_Title "
                + "FROM reservations r, bookcopies bc, books b "
                + "WHERE r.Bc_ID = bc.Bc_ID AND b.Bk_ID = bc.Bk_ID;";
        rs = st.executeQuery(sql);
        
        while (rs.next())
        {
            result = new Vector<>();
            result.add(rs.getString(1));
            result.add(rs.getString(2));
            result.add(rs.getString(3));
            resBooks.add(result);
        }
        st.close();
        db.disconnect();
        return resBooks;
    }

    /**
     * Check out a book (given book copy ID) to a student (given student ID)
     * @param bcid
     * @param sid
     * @param loanPeriod
     * @throws SQLException
     */
    public void checkOut(Integer bcid, String sid, Integer loanPeriod) throws SQLException {
        db.connect();
       
        sql = "INSERT INTO transactions "
                + "(Trans_DateBorrowed, Trans_DateExpected, Stu_ID, Bc_ID) "
                + "VALUES (CURDATE(), "
                + "(SELECT DATE_ADD(CURDATE(), INTERVAL ? WEEK)), "
                + "?, ?);";
        
        PreparedStatement pst = db.getConnect().prepareStatement(sql);
        pst.setInt(1, loanPeriod);
        pst.setString(2, sid);
        pst.setInt(3, bcid);
        pst.executeUpdate();
        
        sql = "UPDATE bookcopies SET Bc_Status = 0 WHERE Bc_ID = ?;";
        pst = db.getConnect().prepareStatement(sql);
        pst.setInt(1, bcid);
        pst.executeUpdate();
        
        pst.close();
        db.disconnect();
    }
 
    /**
     * Check in a book back into the library with the given Book Copy ID
     * @param bcid
     * @throws SQLException
     */
    public void check_in(Integer bcid) throws SQLException {
        db.connect();
        st = db.getConnect().createStatement();
        
        //mark date returned in transaction
        sql = "UPDATE transactions "
                + "SET Trans_DateReturned = CURDATE() "
                + "WHERE Bc_ID = " + bcid + " and Trans_DateReturned is null;";
        st.executeUpdate(sql);
        
        //mark in bookcopies as available
        sql = "UPDATE bookcopies SET Bc_Status = 1 WHERE Bc_ID = " + bcid + ";";
        st.executeUpdate(sql);
        
        //if book checked is in reserved, check it out same time
        //if this bcid is in the reservations table, 
        sql = "SELECT r.Stu_ID, s.type "
                + "FROM reservations r NATURAL JOIN students s "
                + "WHERE r.Bc_ID = " + bcid + " LIMIT 1;";
        rs = st.executeQuery(sql);
        
        if (rs.next())
        {   //then check the book out to that student         
            String sid = rs.getString(1);
            if (rs.getString(2).equals("Undergraduate")) {
                this.checkOut(bcid, sid, 2);
            }
            else{
                this.checkOut(bcid, sid, 4);
            }
            
            //remove from reservation table
            this.deleteReservation(bcid);
        }
        st.close();
        db.disconnect();
    }

    /**
     * Delete a book reservation given the book copy ID
     * @param bcid_delete
     * @throws SQLException
     */
    public void deleteReservation(Integer bcid_delete) throws SQLException {
        db.connect();
        st = db.getConnect().createStatement();
        
        //select the reservation id of the first reservation for that book
        sql = "SELECT MIN(r1.Rs_ID) "
                + "FROM reservations r1 "
                + "WHERE r1.Bc_ID = " + bcid_delete + ";";
        rs = st.executeQuery(sql);
        if (rs.next())
        {
            sql = "DELETE FROM reservations "
                    + "WHERE Rs_ID = " + rs.getInt(1) + ";";
            st.executeUpdate(sql);
        }
        st.close();
        db.disconnect();
    }

    /**
     * Delete a book from the system given the book copy ID
     * @param copyID
     * @throws SQLException
     */
    public void delete(Integer copyID) throws SQLException {
        db.connect();
        st = db.getConnect().createStatement();
        
        sql = "DELETE FROM bookcopies WHERE Bc_ID = " + copyID + ";"; 
        st.executeUpdate(sql);
        st.close();
        db.disconnect();
    }   

    /**
     * Get the next assignable Book Copy ID in the database
     * @param aThis
     * @return the next book copy ID
     * @throws SQLException
     */
    public Integer getNextBCID(BLbookCopy aThis) throws SQLException {
        db.connect();
        st = db.getConnect().createStatement();
        Integer result;
        
        sql = "SELECT max(bc.Bc_ID) FROM bookcopies bc;";
        rs = st.executeQuery(sql);
        rs.next();
        result = rs.getInt(1) + 1;
        
        st.close();
        db.disconnect();
        return result;
    }

    /**
     * Checks whether a given book copy is checked out or not
     * @param book
     * @return true if book is checked out
     * @throws SQLException 
     */
    public boolean isCheckedOut(BLbookCopy book) throws SQLException {
        db.connect();
        st = db.getConnect().createStatement();
        Boolean result = false;
        
        sql = "SELECT t.Stu_ID FROM transactions t "
                + "WHERE t.Trans_DateReturned is null "
                + "AND t.Bc_ID = " + book.getBcid() + ";";
        rs = st.executeQuery(sql);
        if (rs.next())
        {
            result = true;
        }
        st.close();
        db.disconnect();
        
        return result;
    }
}