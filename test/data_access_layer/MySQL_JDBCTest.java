
package data_access_layer;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Abhineet
 */
public class MySQL_JDBCTest {
    
    public MySQL_JDBCTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of connect method, of class MySQL_JDBC.
     * @throws java.lang.Exception
     */
    @Test
    public void testConnect() throws Exception {
        System.out.println("connect");
        MySQL_JDBC instance = new MySQL_JDBC();
        instance.connect();
    }

    /**
     * Test of disconnect method, of class MySQL_JDBC.
     * @throws java.lang.Exception
     */
    @Test
    public void testDisconnect() throws Exception {
        System.out.println("disconnect");
        MySQL_JDBC instance = new MySQL_JDBC();
        instance.connect();
        instance.disconnect();
    }

    /**
     * Test of getConnectionString method, of class MySQL_JDBC.
     */
    @Test
    public void testGetConnectionString() {
        System.out.println("getConnectionString");
        MySQL_JDBC instance = new MySQL_JDBC();
        String expResult = "jdbc:mysql://localhost:3306/lame";
        String result = instance.getConnectionString();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPassword method, of class MySQL_JDBC.
     */
    @Test
    public void testGetPassword() {
        System.out.println("getPassword");
        MySQL_JDBC instance = new MySQL_JDBC();
        String expResult = "temppass";
        String result = instance.getPassword();
        assertEquals(expResult, result);
    }
}
