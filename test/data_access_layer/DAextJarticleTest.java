
package data_access_layer;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Abhineet
 */
public class DAextJarticleTest {
    
    MySQL_JDBC db;
    
    public DAextJarticleTest() {
        db = new MySQL_JDBC();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of search method, of class DAextJarticle for IEEE
     * @throws java.lang.Exception
     */
    @Test
    public void testSearchIEEE() throws Exception {
        System.out.println("Search using IEEE");
        db.setUrl("localhost:3306/ieee");
        db.connect();
    }
    /**
     * Test of search method, of class DAextJarticle, for Springer
     * @throws java.lang.Exception
     */
    @Test
    public void testSearchSpringer() throws Exception {
        System.out.println("Search using Springer");
        db.setUrl("localhost:3306/springer");
        db.connect();
    }
    
}
