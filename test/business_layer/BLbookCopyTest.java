package business_layer;

import java.sql.SQLException;
import java.util.Vector;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author Abhineet
 */
public class BLbookCopyTest {
    BLbookCopy bookc;
    
    public BLbookCopyTest(){
        bookc = new BLbookCopy();
        bookc.setBcid(0);
        
    }
    @BeforeClass
    public static void setUpClass() {
        System.out.println("Setting up a fake bookcopy with ID '0'.");   
    }

    /**
     * Test of getReservedBooks method, of class BLbookCopy.
     * @throws java.lang.Exception
     */
    @Test
    public void testGetReservedBooks() throws Exception {
        System.out.println("Testing 'Get Reserved Books' - "
                + "there should be at least one reserved book for this to work");
        BLbookCopy instance = new BLbookCopy();
        Vector expResult = new Vector<>();
        Vector result = instance.getReservedBooks();
        assertFalse(expResult.size() == result.size());
    }

    /**
     * Test of check_in method, of class BLbookCopy.
     * @throws java.lang.Exception
     */
    @Test
    public void testCheck_in() throws Exception {
        System.out.println("Testing Check-in");
        BLbookCopy instance = new BLbookCopy();
        instance.check_in(this.bookc.getBcid());
    }

    /**
     * Test of checkOut method, of class BLbookCopy.
     */
    @Test
    public void testCheckOut() {
        System.out.println("checkOut");
        Integer bcid = 0;
        String sid = "";
        Integer loanPeriod = 1;
        BLbookCopy instance = new BLbookCopy();
        try {
            instance.checkOut(bcid, sid, loanPeriod);
            System.out.println("No Exceptions returned - test failed.");
            assertTrue(false);
        } catch (SQLException ex) {
            System.out.println("SQL Exception returned - test passed!");
            assertTrue(true);
        }
    }

    /**
     * Test of deleteReservation method, of class BLbookCopy.
     * @throws java.lang.Exception
     */
    @Test
    public void testDeleteReservation() throws Exception {
        System.out.println("deleteReservation");
        Integer bcid_delete = null;
        BLbookCopy instance = new BLbookCopy();
        instance.deleteReservation(bcid_delete);
    }

    /**
     * Test of delete method, of class BLbookCopy.
     * @throws java.lang.Exception
     */
    @Test
    public void testDelete() throws Exception {
        System.out.println("delete");
        BLbookCopy instance = new BLbookCopy();
        instance.setBcid(0);
        instance.delete();
    }

    /**
     * Test of getNextBCID method, of class BLbookCopy.
     * @throws java.lang.Exception
     */
    @Test
    public void testGetNextBCID() throws Exception {
        System.out.println("getNextBCID");
        BLbookCopy instance = new BLbookCopy();
        Integer expResult = 18;
        Integer result = instance.getNextBCID();
        assertEquals(expResult, result);
    }

    /**
     * Test of isCheckedOut method, of class BLbookCopy.
     * @throws java.lang.Exception
     */
    @Test
    public void testIsCheckedOut() throws Exception {
        System.out.println("isCheckedOut");
        boolean expResult = false;
        boolean result = bookc.isCheckedOut();
        assertEquals(expResult, result);
    }   
}