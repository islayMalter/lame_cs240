/**
@author Abhineet Gupta (S11058338)
@author Wafaa Wardah (S11042586)
@author Ravishel Naicker (S11098100)

CS240: Software Engineering
@since Sem 1, 2014 @ USP
Library Asset Management Enabler (LAME) System
*/

Default Credentials:
	Student
		UN: s11058338
		PW: temppass

	Clerk
		UN: test
		PW: pass
	
Build Instructions:	
	Open project in NetBeans IDE.
	Run.